# Activity 13: Test-Driven Development

## Content Learning Objectives

After completing this activity, students should be able to:

## Process Skill Goals

During the activity, students should make progress toward:

- Leveraging prior knowledge and experience of other students. (Teamwork)

## Team Roles

Decide what role each of you will play for today. Choose a role that you have not played before, or recently. The goal should be to have all team members rotate through the roles on a regular basis to become comfortable with all the roles. If you have only three people, one should have two roles. If you have five people, two may share the same role. Record role assignments here.

Role | Team Member
--- | ---
Manager/Reflector |
Presenter |
Recorder |
Technician |

The Technician’s role is use a computer to operate the IDE, enter the code, and run the tests.

## Model 1: How to do Test-Driven Development

Read, as a team, Kent Beck's post [Canon TDD](https://tidyfirst.substack.com/p/canon-tdd) 

Especially pay attention to:

- The 5 steps
- The flowchart *including the mistakes comments*.
- The *Overview*
- The Interface/Implemenation Split
- The detailed descriptions of the 5 steps.

## Model 2: Doing Test-Driven Development

1. Create a `testlist.md` file.
    - Use `-` at the beginning of each line to make it into a list element.
    - Surround list element text with `~~` on each end to cross out tests that you have completed.
2. Create test file(s) in `src/test/java`
3. Create implementation code file(s) in `src/main/java`
   
## Model 3: The Kata

Do the [Simple Mars Rover Kata](https://www.codurance.com/katas/simple-mars-rover)



---

&copy; 2024 Karl R. Wurst <karl@w-sts.com>

<!-- markdownlint-disable MD033 -->
<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
